#! /bin/bash

cp /opt/docker/ldap.conf /etc/openldap/ldap.conf
#cp /opt/docker/nscd.conf /etc/nscd.conf
cp /opt/docker/nslcd.conf /etc/nslcd.conf
cp /opt/docker/nsswitch.conf /etc/nsswitch.conf

#iniciacio dels serveis nscd nslcd
/sbin/nscd &> /dev/null && echo "nscd engegat"
/sbin/nslcd &> /dev/null && echo "nslcd engegat"
cp /opt/docker/system-auth /etc/pam.d/system-auth

#Creacio d'usuaris locals
useradd unix01
useradd unix02
useradd unix03
echo -e "unix01\nunix01\n" | passwd --stdin unix01 > /dev/null
echo -e "unix02\nunix02\n" | passwd --stdin unix02 > /dev/null
echo -e "unix03\nunix03\n" | passwd --stdin unix03 > /dev/null
#echo "CHFN_RESTRICT no" >> /etc/login.defs
/bin/bash
